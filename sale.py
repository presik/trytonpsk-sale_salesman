# This file is part of Tryton.  The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
import polars as pl
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    salesman = fields.Many2One('company.employee', 'Salesman', states={
        'readonly': Eval('state').in_(['confirmed', 'processing', 'done']),
    })

    def _get_invoice_sale(self):
        invoice = super(Sale, self)._get_invoice_sale()

        if self.salesman:
            invoice.salesman = self.salesman
        return invoice

    def on_change_party(self, name=None):
        super(Sale, self).on_change_party()
        if self.party and self.party.salesman:
            self.salesman = self.party.salesman


class SaleDetailedStart(metaclass=PoolMeta):
    "Sale Detailed Start"
    __name__ = 'sale_shop.sale_detailed.start'
    salesmans = fields.Many2Many('company.employee', None, None, 'Salesmans')


class SaleDetailed(metaclass=PoolMeta):
    "Sale Detailed"
    __name__ = 'sale_shop.sale_detailed'

    def do_print_(self, action):
        action, data = super(SaleDetailed, self).do_print_(action)
        salesmans = [s.id for s in self.start.salesmans]
        data['salesmans'] = salesmans
        return action, data


class SaleDetailedReport(metaclass=PoolMeta):
    "Sale Shop Sale Detailed Report"
    __name__ = 'sale_shop.sale_detailed_report'

    @classmethod
    def get_where(cls, data):
        where = super(SaleDetailedReport, cls).get_where(data)
        salesmans = data.get('salesmans')
        if len(salesmans) == 1:
            where += f' AND i.salesman={salesmans[0]} '
        elif salesmans:
            where += f' AND i.salesman IN {tuple(salesmans)!s} '
        return where

    @classmethod
    def get_data_query(cls, data):
        df = super(SaleDetailedReport, cls).get_data_query(data)
        cursor = Transaction().connection.cursor()
        from_invoice = data.get('from_invoice')
        table = 'sale_sale'
        column_table = 'sale'
        states = cls.get_states(data)
        where = cls.get_where(data)

        if from_invoice:
            table = 'account_invoice'
            column_table = 'invoice'

        query_salesman = """
            SELECT i.id as record_sale, p.name as salesman_name
            FROM company_employee AS e
            LEFT JOIN {table} AS i ON e.id=i.salesman
            LEFT JOIN party_party AS p ON e.party=p.id
            WHERE i.state in {states}
            AND i.{column_table}_date >= '{start_date}'
            AND i.{column_table}_date <= '{end_date}'
            {where};
        """

        query_salesman = query_salesman.format(
            table=table, column_table=column_table,
            start_date=data['start_date'],
            end_date=data['end_date'],
            states=states,
            where=where)

        schema = {
            'record_sale': pl.Int64,
            'salesman_name': pl.String,
        }

        df_salesman = pl.read_database(query_salesman, cursor, schema_overrides=schema)
        df = df.join(df_salesman, on='record_sale', how="left")
        return df


class SaleByMonthStart(metaclass=PoolMeta):
    "Sale By Month Start"
    __name__ = 'sale_shop.sale_by_month.start'

    salesmans = fields.Many2Many('company.employee', None, None, 'Salesmans')

    @classmethod
    def get_selection_group_by(cls):
        group_by = super(SaleByMonthStart, cls).get_selection_group_by()
        group_by.append(('salesman', 'Salesman'))
        return group_by


class SaleByMonth(metaclass=PoolMeta):
    "Sale By Month"
    __name__ = 'sale_shop.sale_by_month.wizard'

    def do_print_(self, action):
        action, data = super(SaleByMonth, self).do_print_(action)
        if self.start.salesmans:
            data['salesmans'] = [s.id for s in self.start.salesmans]
        return action, data


class SaleByMonthReport(metaclass=PoolMeta):
    __name__ = 'sale_shop.sale_by_month_report'

    @classmethod
    def get_other_where(cls, data):
        other_where = super(SaleByMonthReport, cls).get_other_where(data)
        salesmans = ''
        if data.get('salesmans'):
            field_salesmans = tuple(data['salesmans'])
            if len(field_salesmans) == 1:
                field_salesmans = str(field_salesmans).replace(',', '')
            salesmans = f" and a.salesman in {field_salesmans}"
        return other_where + salesmans
