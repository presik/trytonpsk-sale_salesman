# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# repository contains the full copyright notices and license terms.
from trytond.pool import Pool

from . import invoice, party, sale


def register():
    Pool.register(
        sale.Sale,
        party.Party,
        invoice.Invoice,
        party.PortfolioDetailedStart,
        sale.SaleDetailedStart,
        sale.SaleByMonthStart,
        module='sale_salesman', type_='model')
    Pool.register(
        sale.SaleDetailed,
        sale.SaleByMonth,
        module='sale_salesman', type_='wizard')
    Pool.register(
        sale.SaleDetailedReport,
        sale.SaleByMonthReport,
        module='sale_salesman', type_='report')
