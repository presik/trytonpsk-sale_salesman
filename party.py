# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    salesman = fields.Many2One('company.employee', 'Salesman')

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()


class PortfolioDetailedStart(metaclass=PoolMeta):
    __name__ = 'invoice_report.portfolio_detailed.start'
    sellers = fields.Many2Many('company.employee', None, None, 'Sellers')

    @classmethod
    def __setup__(cls):
        super(PortfolioDetailedStart, cls).__setup__()
        cls.grouped.selection.append(('salesman', 'Salesman'))
